core_deps:
  pkg.installed:
    - pkgs:
      - git

sunpowered:
  user.present:
    - shell: /bin/bash

/var/log/sunpowered:
  file.directory:
    - user: sunpowered
    - group: sunpowered

/home/sunpowered/.ssh/id_rsa:
  file.managed:
    - makedirs: True
    - source: salt://bb_webhooks/files/pillar_key
    - template: jinja
    - context:
      key: sunpowered:ssh:key
    - user: sunpowered
    - group: sunpowered
    - mode: 600

/home/sunpowered/.ssh/id_rsa.pub:
  file.managed:
    - makedirs: True
    - source: salt://bb_webhooks/files/key
    - template: jinja
    - context:
      key: {{ salt['pillar.get']('sunpowered:ssh:pub', '') }}
    - user: sunpowered
    - group: sunpowered
    - mode: 644
