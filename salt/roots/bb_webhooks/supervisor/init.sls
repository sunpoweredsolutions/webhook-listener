supervisor:
  pkg.installed: []
  service.running:
    - watch:
      - file: /etc/supervisor/conf.d/webhooks.conf
    - require:
      - pkg: supervisor

/etc/supervisor/conf.d/webhooks.conf:
   file.managed:
     - source: salt://bb_webhooks/supervisor/files/webhooks.supervisor
     - user: root
     - group: root
     - require:
       - pkg: supervisor

bb_webhooks:
  supervisord.running:
    - update: True
    - watch:
      - file: /etc/supervisor/conf.d/webhooks.conf
    - require:
      - virtualenv: /home/sunpowered/.virtualenvs/webhooks
