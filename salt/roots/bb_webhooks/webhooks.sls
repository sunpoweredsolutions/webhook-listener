webhooks_deps:
  pkg.installed:
    - pkgs:
      - python-dev
      - python-virtualenv
      - python-pip

bitbucket.org:
  ssh_known_hosts.present:
    - user: sunpowered
    - fingerprint: 97:8c:1b:f2:6f:14:6b:5c:3b:ec:aa:46:46:74:7c:40

/home/sunpowered/webhook-listener:
  file.symlink:
    - target: /vagrant
    - user: sunpowered
    - group: sunpowered

/home/sunpowered/.virtualenvs/webhooks:
  virtualenv.managed:
    - requirements: salt://bb_webhooks/files/REQUIREMENTS.txt
    - user: sunpowered

/home/sunpowered/.webhooks:
  file.managed:
    - source: salt://bb_webhooks/files/webhooks
    - user: sunpowered
    - group: sunpowered

/var/repo:
  file.directory:
    - user: sunpowered

test_project:
  git.latest:
    - name: git@bitbucket.org:sunpoweredsolutions/test-project.git
    - target: /var/repo/test-project
    - user: sunpowered
    - require:
      - file: /home/sunpowered/.ssh/id_rsa
      - ssh_known_hosts: bitbucket.org