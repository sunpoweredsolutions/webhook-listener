nginx:
  pkg.installed: []
  service:
    - running
    - require:
      - pkg: nginx
    - watch:
      - file: /etc/nginx/sites-available/webhooks

/etc/nginx/sites-available/webhooks:
  file.managed:
    - source: salt://bb_webhooks/nginx/files/webhooks.nginx
    - template: jinja
    - require:
      - pkg: nginx

/etc/nginx/sites-enabled/default:
  file.absent:
    - require:
      - pkg: nginx

/etc/nginx/sites-enabled/webhooks:
  file.symlink:
    - target: /etc/nginx/sites-available/webhooks
    - require:
      - pkg: nginx
    - mode: 644
    - user: www-data
    - group: www-data
