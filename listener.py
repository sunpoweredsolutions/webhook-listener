import subprocess
import logging

from flask import Flask  
from flask import request  

import git

FETCH_REPO_CMD='fetch_repo'
USE_GITPYTHON = True

class Config:
    GIT_REPOS = {}
    DEBUG = False

app = Flask(__name__)
app.config.from_object(Config)
app.config.from_envvar("SP_WEBHOOKS_CFG")


def configure_logger(app):
    logger = app.logger
    logger.setLevel(logging.DEBUG)
    fmt = '%(asctime)s:%(levelname)s:%(message)s'
    hndlr = logging.StreamHandler()
    hndlr.setLevel(logging.INFO)
    hndlr.setFormatter(logging.Formatter(fmt))
    logger.addHandler(hndlr)

configure_logger(app)

def displayIntro():
    app.logger.info( 'Webhook server online! Go to http://localhost:5000' )

def displayHTML(request):
    return 'Webhook server online! Go to <a href="https://bitbucket.com">Bitbucket</a> to configure your repository webhook for <a href="%swebhook">%swebhook</a>' % (request.url_root,request.url_root)

def pull_git_repo(folder):
    if USE_GITPYTHON:
        return pull_git_repo_gitpython(folder)
    else:
        return pull_git_repo_external_script(folder)

def pull_git_repo_external_script(folder):
    subprocess.call([FETCH_REPO_CMD, folder])

def pull_git_repo_gitpython(folder):
    if folder:
        g = git.cmd.Git(folder)
        if g:
            app.logger.info("Updating git repo: {}".format(folder))
            try:
                g.fetch()
                g.pull()
            except Exception as e:
                app.logger.error("{}: {}".format(e.__class__.__name__, str(e)))
            else:
                app.logger.info("Success")

@app.route('/', methods=['GET'])
def index():  
    return displayHTML(request)

@app.route('/webhook', methods=['GET', 'POST'])
def tracking():
    import getpass
    if request.method == 'POST':
        data = request.get_json()

        try:
            new_data = data['push']['changes'][0]['new']
            branch_name = new_data['name']
            commit_author = data['actor']['username']
            commit_hash = new_data['target']['hash'][:7]
            #commit_url = new_data['target']['links']['html']['href']
            repo_name = data['repository']['name']
            # Show notification if operating system is OS X
        except (ValueError, KeyError, TypeError) as e:
            app.logger.error("Error in POST data: {}".format(str(e)))
            return "Nope"
        else:
            app.logger.info("Webhook hit!  Repo: {} {} by {}".format(repo_name, commit_hash, commit_author))
            #app.logger.info( 'Webhook received! %s committed %s' % (commit_author, commit_hash) )
            #app.logger.info( "Running as user: {}".format(getpass.getuser()))
            pull_git_repo(app.config['GIT_REPOS'].get(repo_name, None))
            return 'OK'
    else:
        return displayHTML(request)

if __name__ == '__main__':
    #displayIntro()
    app.logger.info("Starting SunPowered Web Hooks")
    app.logger.info("Configured to listen to the following repositories:")
    app.logger.info(app.config['GIT_REPOS'])

    app.run(host='127.0.0.1', port=5000, debug=app.debug, use_reloader=False)
