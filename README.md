This repository sets up a simple Flask server to receive [Bitbucket](https://bitbucket.com) webhooks and looks for 
configured git repos to pull.

Configuration locally is done via setting the `SP_WEBHOOKS_CFG` variable to a config file.  The config file should
set a `GIT_REPOS` variable to a dict of (`repo_name`, `local_dir`) key, value pairs.  Directing hooks from a given
`repo_name` to a local git directory, `local_dir`.  The script then tries to fetch and pull from the git remote.
